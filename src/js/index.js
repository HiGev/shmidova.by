
/* Цвет меню при скроле */

var navColor = document.querySelector('.navbar');
document.addEventListener('scroll',showNav);
function showNav(){
    if(window.pageYOffset>=300){
        navColor.style.backgroundColor="#191919f6"
    }
    else{
        navColor.style.backgroundColor="#19191900"
    }
}

/* Жирный текст меню при клике */

var firstBtn= document.querySelector(".one");
var secondBtn= document.querySelector(".two");
var thirdBtn= document.querySelector(".three");
var foursBtn= document.querySelector(".four");
var fivesBtn= document.querySelector(".five");

var navBtnColor = document.querySelectorAll('.header__menu');
 for(var i = 0; i< navBtnColor.length; i++) {
    navBtnColor[i].addEventListener('click', function(btnColor){
        for(var x = 0; x<navBtnColor.length; x++) {
            if(navBtnColor[x] == event.currentTarget) {
                navBtnColor[x].style.fontWeight="bold";
            } else {
                navBtnColor[x].style.fontWeight="normal";
            }
        }
    })
}

/* Переключение Секций на кнопки меню */

var firstScreen = document.querySelector(".aboutus");
firstBtn.addEventListener('click',showFirstScreen);
function showFirstScreen (){
    firstScreen.scrollIntoView();
    firstScreen.scrollIntoView(true);
    firstScreen.scrollIntoView({block: "end"});
    firstScreen.scrollIntoView({behavior: "instant", block: "start", inline: "nearest"});
}


var secondScreen = document.querySelector(".aboutme");
secondBtn.addEventListener('click',showSecondScreen);
function showSecondScreen (){
    secondScreen.scrollIntoView();
    secondScreen.scrollIntoView(true);
    secondScreen.scrollIntoView({block: "strat"});
    secondScreen.scrollIntoView({behavior: "instant", block: "start", inline: "nearest"});
}


var thirdScreen = document.querySelector(".price");
thirdBtn.addEventListener('click',showThirdScreen);
function showThirdScreen (){
    thirdScreen.scrollIntoView();
    thirdScreen.scrollIntoView(true);
    thirdScreen.scrollIntoView({block: "center"});
    thirdScreen.scrollIntoView({behavior: "instant", block: "start", inline: "nearest"});
}


var foursScreen = document.querySelector(".gift");
foursBtn.addEventListener('click',showFoursScreen);
function showFoursScreen (){
    foursScreen.scrollIntoView();
    foursScreen.scrollIntoView(true);
    foursScreen.scrollIntoView({block: "end"});
    foursScreen.scrollIntoView({behavior: "instant", block: "start", inline: "nearest"});
}


var fivesScreen = document.querySelector(".footer");
fivesBtn.addEventListener('click',showFivesScreen);
function showFivesScreen (){
    fivesScreen.scrollIntoView();
    fivesScreen.scrollIntoView(true);
    fivesScreen.scrollIntoView({block: "end"});
    fivesScreen.scrollIntoView({behavior: "instant", block: "start", inline: "nearest"});
}

/* Переключение на первую секцию */

var arrowBtn = document.querySelector('.svg');

var firstScreen = document.querySelector(".aboutus");
arrowBtn.addEventListener('click',showFirstScreen);
function showFirstScreen (){
    firstScreen.scrollIntoView();
    firstScreen.scrollIntoView(true);
    firstScreen.scrollIntoView({block: "end"});
    firstScreen.scrollIntoView({behavior: "instant", block: "start", inline: "nearest"});
}

/* Открывает форму для заказа звонка по клику */

var callForm = document.querySelector('.call');
var cancelBtn = document.querySelector('.close');

var phoneBtn = document.querySelector('.phone');
phoneBtn.addEventListener('click',showCallForm);
function showCallForm (){
    phoneBtn.style.display="none";
    callForm.style.display="flex";
    cancelBtn.style.display="block";
}

cancelBtn.addEventListener('click',hideCallForm);
function hideCallForm (){
    phoneBtn.style.display="block";
    callForm.style.display="none";
    cancelBtn.style.display="none";
}

/*  Кнопка для записи на занятие */


var sendForm = document.querySelector('.banner');
var closeBtn = document.querySelector('.banner__button--close');

var enteryBtn = document.querySelector('.header__button');
enteryBtn.addEventListener('click',showSendForm);
function showSendForm (){
    sendForm.style.display="flex";
    navColor.style.display="none";
    phoneBtn.style.display="none";
}

closeBtn.addEventListener('click',hideSendForm);
function hideSendForm (){
    sendForm.style.display="none";
    navColor.style.display="flex";
    phoneBtn.style.display="block";
}


/* Слайдер галерея */

 var allSlides = document.querySelectorAll('.slider__item');

allSlides[0].style.width = '100%';
allSlides[0].style.opacity = '1';




var nextBtn = document.querySelector('.right');

nextBtn.addEventListener('click', nextSlide);
// по клику на кнопку next
function nextSlide() {
    for(var i = 0; i<allSlides.length; i++) {
        if(allSlides[i].style.width == "100%") {
            if(i == allSlides.length-1) {
                allSlides[i].style.width = "0px";
                allSlides[0].style.width = "100%";
                allSlides[i].style.opacity = "0";
                allSlides[0].style.opacity = "1";  
                break;
            }
            allSlides[i].style.width = "0px";
            allSlides[i+1].style.width = "100%";
            allSlides[i].style.opacity = "0";
            allSlides[i+1].style.opacity = "1";  
            break;
        }
    }
}
var prewBtn = document.querySelector('.left');

prewBtn.addEventListener('click', prewSlide);
// по клику на кнопку prew
function prewSlide() {
    for(var i = 0; i<allSlides.length; i++) {
        if(allSlides[i].style.width == "100%") { 
            if(i == 0) { 
            allSlides[i].style.width = "0px"; 
            allSlides[allSlides.length - 1].style.width = "100%"; 
            allSlides[i].style.opacity = "0"; 
            allSlides[allSlides.length - 1].style.opacity = "1"; 
            break; 
            } 
            allSlides[i].style.width = "0px"; 
            allSlides[i-1].style.width="100%"; 
            allSlides[i].style.opacity = "0"; 
            allSlides[i-1].style.opacity = "1"; 
            break; 
            } 
     } 
}


/* секция aboutus анимация при скроле*/


document.addEventListener('scroll',aboutShow);
 var aboutTitleShow =document.querySelector(".title--first");
function aboutShow(){
    if(window.pageYOffset>=600){
        aboutTitleShow.style.opacity="1";
        aboutTitleShow.style.width="48%";
    }
    else{
        aboutTitleShow.style.opacity="0";
        aboutTitleShow.style.width="0";
    }
} 

document.addEventListener('scroll',aboutShowTwo);
 var aboutSecondTitleShow =document.querySelector(".title--second");
function aboutShowTwo(){
    if(window.pageYOffset>=900){
        aboutSecondTitleShow.style.opacity="1";
        aboutSecondTitleShow.style.width="48%"
    }
    else{
        aboutSecondTitleShow.style.opacity="0";
        aboutSecondTitleShow.style.width="0";
    }
} 


/* секция you анимация при скроле*/


document.addEventListener('scroll',youShow);
 var youTitleShow =document.querySelector(".you__maintitle");
function youShow(){
    if(window.pageYOffset>=2900){
        youTitleShow.style.opacity="1";
        youTitleShow.style.marginBottom="0px";
    }
    else{
        youTitleShow.style.opacity="0";
        youTitleShow.style.marginBottom="40px";
    }
} 

document.addEventListener('scroll',youItemShow);
 var youItemsShow =document.querySelector(".you__items");
function youItemShow(){
    if(window.pageYOffset>=3300){
        youItemsShow.style.opacity="1";
    }
    else{
        youItemsShow.style.opacity="0";
    }
} 


/* секция you анимация при скроле*/


document.addEventListener('scroll',coursesShow);
 var coursesTitleShow =document.querySelector(".courses__maintitle");
function coursesShow(){
    if(window.pageYOffset>=3800){
        coursesTitleShow.style.opacity="1";
    }
    else{
        coursesTitleShow.style.opacity="0";
    }
} 


document.addEventListener('scroll',coursesContantShow);
 var coursesItemShow =document.querySelector(".courses__content");
function coursesContantShow(){
    if(window.pageYOffset>=3800){
        coursesItemShow.style.opacity="1";
        coursesItemShow.style.width="70%";
    }
    else{
        coursesItemShow.style.opacity="0";
        coursesItemShow.style.width="0";
    }
} 

/* секция gallery анимация при скроле*/


document.addEventListener('scroll',galleryShow);
 var galleryShow =document.querySelector(".gallery__maintitle");
function galleryShow(){
    if(window.pageYOffset>=4300){
        galleryShow.style.opacity="1";
    }
    else{
        galleryShow.style.opacity="0";
    }
} 


document.addEventListener('scroll',galleryTextShow);
 var galleryItemShow =document.querySelector(".gallery__title");
function galleryTextShow(){
    if(window.pageYOffset>=5000){
        galleryItemShow.style.opacity="1";
    }
    else{
        galleryItemShow.style.opacity="0";
    }
}
document.addEventListener('scroll',galleryImgShow);
 var galleryImgShow =document.querySelector(".gallery__item");
function galleryImgShow(){
    if(window.pageYOffset>=5000){
        galleryImgShow.style.opacity="1";
    }
    else{
        galleryImgShow.style.opacity="0";
    }
}

document.addEventListener('scroll',galleryImgTitleShow);
 var galleryTitleImgShow =document.querySelector(".gallery__title--second");
function galleryImgTitleShow(){
    if(window.pageYOffset>=5000){
        galleryTitleImgShow.style.opacity="1";
    }
    else{
        galleryTitleImgShow.style.opacity="0";
    }
}



/* секция price анимация при скроле*/

document.addEventListener('scroll',priceShow);
 var priceShow =document.querySelector(".price__maintitle");
function priceShow(){
    if(window.pageYOffset>=6600){
        priceShow.style.opacity="1";
    }
    else{
        priceShow.style.opacity="0";
    }
}

document.addEventListener('scroll',priceItemShow);
 var priceItemShow =document.querySelector(".price__content");
function priceItemShow(){
    if(window.pageYOffset>=6800){
        priceItemShow.style.width="auto";
        priceItemShow.style.opacity="1";
    }
    else{
        priceItemShow.style.width="0";
        priceItemShow.style.opacity="0";
    }
}